from pages.BasePage import BasePage
import logging
from selenium.webdriver.common.by import By
from time import sleep


class WelcomePage(BasePage):

    sign_in_button = (By.CLASS_NAME, 'gmail-nav__nav-link__sign-in')

    def click_sign_in_button(self):

        logging.info("click Sign In button")
        sign_in_button = self.driver.find_element(*WelcomePage.sign_in_button)
        sign_in_button.click()
        sleep(1)
