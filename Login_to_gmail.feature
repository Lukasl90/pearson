# Story-01 Title:  Login to  gmail.com

# Narration:
#   As registered gmail user I want to login to gmail So I can see my emails

# Preconditions:
#   User has google account

# Acceptance Criteria:
#   User is informed about wrong credentials
#   User after successful login is redirected to gmail homepage


Feature: Login to gmail.com.
    As registered gmail user I want to login to gmail, so I can see my emails

    Scenario: Login to gmail.com
        Given user has google account
        When user login to gmail with correct credentials
        Then user is redirected to gmail homepage
        And user can see its own emails

    Scenario: Login to gmail.com with wrong credentials
        Given user has google account
        When user login to gmail with wrong credentials
        Then user is informed about wrong credentials

    Scenario: Login to gmail.com without password
        Given user has google account
        When user login to gmail without password
        Then user is informed about unfilled password


