""" Test Case: Login into gmail.com """

import unittest
import logging
from setup import Setup
from pages.BasePage import BasePage
from pages.WelcomePage import WelcomePage
from pages.SignInPage import SignInPage
from pages.GmailHomePage import GmailHomePage


class TestLoginIntoGmail(Setup):

    def setUp(self):

        self.setup = Setup()
        self.driver = self.setup.start_browser()

    def tearDown(self):

        self.setup.stop()

    def test_login_into_gmail(self):

        logging.info("starting Test Case: Login into gmail account")
        base_page = BasePage(self.driver)
        base_page.open_url('https://www.google.com/gmail/about/#')
        welcome_page = WelcomePage(self.driver)
        welcome_page.click_sign_in_button()
        sign_in_page = SignInPage(self.driver)
        sign_in_page.type_username("testspearson1990")
        sign_in_page.click_next_button()
        sign_in_page.type_password("pearson12345")
        sign_in_page.click_next_button()
        gmail_homepage = GmailHomePage(self.driver)
        gmail_homepage.verify_url()


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestLoginIntoGmail)
    unittest.TextTestRunner(verbosity=2).run(suite)
