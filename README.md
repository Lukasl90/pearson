# Recruitment task for company Pearson


### Recruitment tasks:

1. Using Gherkin write acceptance tests for following user stories:

**Story-01 Title: Login to gmail.com**

- Narration:
As registered gmail user I want to login to gmail So I can see my emails

- Preconditions:
User has google account

- Acceptance Criteria:
User is informed about wrong credentials
User after successful login is redirected to gmail homepage

**Story-02 Title: Send email**

- Narration:
As logged user I want to send email So I can communicate with others

- Acceptance Criteria:
User should be able to send attachments
User should be able to send empty messages
Sent email should be available in Send category

2. Write additional test scenarios for features described in point 1.

3. Using the Page Object pattern, create a structure and tests for sending an
email (using gmail). The tests have to be executable on any environment.
Implemented code has to be delivered on an outside repository, for example github.
