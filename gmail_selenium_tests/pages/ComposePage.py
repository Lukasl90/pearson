from pages.BasePage import BasePage
import logging
from selenium.webdriver.common.by import By
from time import sleep


class ComposePage(BasePage):

    recipient_field = (By.ID, ':9d')
    # title_field = (By.CLASS_NAME, 'input[name="subjectbox"]')
    title_field = (By.ID, ':8w')
    msg_field = (By.ID, ':9x')
    send_button = (By.ID, ':8m')

    def type_recipient_address(self, recipient_email_address):

        logging.info("type recipient address")
        recipient_input_field = self.driver.find_element(*ComposePage.recipient_field)
        recipient_input_field.send_keys(recipient_email_address)

    def type_title(self, title):

        logging.info("type title for new message")
        title_input_field = self.driver.find_element(*ComposePage.title_field)
        title_input_field.send_keys(title)

    def type_message(self, msg):

        logging.info("type new message")
        msg_input_field = self.driver.find_element(*ComposePage.msg_field)
        msg_input_field.send_keys(msg)

    def click_send_button(self):

        logging.info("click Send button")
        send_button = self.driver.find_element(*ComposePage.send_button)
        send_button.click()
        sleep(1)
