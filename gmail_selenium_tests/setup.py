import os
import platform
import unittest
from selenium import webdriver
from time import sleep
import logging
PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))


class Setup(unittest.TestCase):

    def start_browser(self):

        logging.info("starting browser")

        options = webdriver.ChromeOptions()
        # options.add_argument('headless')
        if platform.system() == "Windows":
            self.driver = webdriver.Chrome(executable_path=PROJECT_ROOT + "\chromedriver_from_google.exe")
        elif platform.system() == "Linux":
            self.driver = webdriver.Chrome(executable_path=PROJECT_ROOT + "/chromedriver_from_google_for_linux")
        else:
            self.driver = webdriver.Chrome(executable_path=PROJECT_ROOT + "/chromedriver_from_google",
                                           chrome_options=options)

        self.driver.maximize_window()
        return self.driver

    def stop(self):

        logging.info("quitting")
        sleep(1)
        self.driver.quit()



