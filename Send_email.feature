# Story-02 Title: Send email

# Narration:
#   As logged user I want to send email So I can communicate with others

# Acceptance Criteria:
#   User should be able to send attachments
#   User should be able to send empty messages
#   Sent email should be available in Send category


Feature: Send email.
    As logged user I want to send email so I can communicate with others

    Scenario: Send email
        Given user has gmail account
        When user is logged in into gmail account
        Then user can send emails
        And sent emails should be available in Send category

    Scenario: Send email with attachments
        Given user has gmail account
        When user is logged in into gmail account
        Then user can send email with attachments
        And sent email should be available in Send category

    Scenario: Send email with empty message
        Given user has gmail account
        When user is logged in into gmail account
        Then user can send email with empty message
        And sent email should be available in Send category

    Scenario: Send email without title
        Given user has gmail account
        When user is logged in into gmail account
        Then user can send email without title
        But user is informed about lack of title
        And sent email should be available in Send category

