from pages.BasePage import BasePage
from selenium.webdriver.common.by import By
import logging
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.ui import WebDriverWait


class SentPage(BasePage):

    def verify_text_presence(self, text):

        text_on_page = (By.XPATH, '//*[text()="%s"]' % text)

        logging.info("verify text presence on page")

        WebDriverWait(self.driver, 4).until(
            expected_conditions.presence_of_element_located(text_on_page),
            "%s - text not found" % text)


