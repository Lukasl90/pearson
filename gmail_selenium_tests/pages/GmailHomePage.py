from pages.BasePage import BasePage
from selenium.webdriver.common.by import By
import logging


class GmailHomePage(BasePage):

    compose_button = (By.CSS_SELECTOR, 'div[class="T-I J-J5-Ji T-I-KE L3"]')
    sent_category = (By.CSS_SELECTOR, 'a[href="https://mail.google.com/mail/#sent"]')

    def click_compose_button(self):

        logging.info("click Compose button")
        compose_button = self.driver.find_element(*GmailHomePage.compose_button)
        compose_button.click()

    def verify_url(self):

        logging.info('verify if current URL is equal to "https://mail.google.com/mail/#inbox" ')
        url = self.driver.current_url
        assert url == "https://mail.google.com/mail/#inbox"

    def open_sent_category(self):

        logging.info("open Sent category")
        sent = self.driver.find_element(*GmailHomePage.sent_category)
        sent.click()
