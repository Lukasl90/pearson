from pages.BasePage import BasePage
import logging
from selenium.webdriver.common.by import By
from time import sleep


class SignInPage(BasePage):

    username_field = (By.ID, 'identifierId')
    password_field = (By.CSS_SELECTOR, 'input[type="password"]')
    next_button = (By.CLASS_NAME, 'RveJvd')

    # def __init__(self, driver):
    #     super(SignInPage, self).__init__(driver)

    def type_username(self, username):

        logging.info("type username")
        username_field = self.driver.find_element(*self.username_field)
        username_field.send_keys(username)

    def type_password(self, password):

        logging.info("type password")
        username_field = self.driver.find_element(*self.password_field)
        username_field.send_keys(password)

    def click_next_button(self):

        logging.info("click Next button")
        next_button = self.driver.find_element(*self.next_button)
        next_button.click()
        sleep(1)

