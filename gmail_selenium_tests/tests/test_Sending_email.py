""" Test Case: Sending email using gmail.com """

import unittest
import logging
from setup import Setup
from pages.BasePage import BasePage
from pages.WelcomePage import WelcomePage
from pages.SignInPage import SignInPage
from pages.GmailHomePage import GmailHomePage
from pages.ComposePage import ComposePage
from pages.SentPage import SentPage


class TestSendingEmail(Setup):

    def setUp(self):

        self.setup = Setup()
        self.driver = self.setup.start_browser()

    def tearDown(self):

        self.setup.stop()

    def test_sending_email(self):

        logging.info("starting Test Case: Sending Email using gmail account")
        base_page = BasePage(self.driver)
        base_page.open_url('https://www.google.com/gmail/about/#')
        welcome_page = WelcomePage(self.driver)
        welcome_page.click_sign_in_button()
        sign_in_page = SignInPage(self.driver)
        sign_in_page.type_username("testspearson1990")
        sign_in_page.click_next_button()
        sign_in_page.type_password("pearson12345")
        sign_in_page.click_next_button()
        gmail_homepage = GmailHomePage(self.driver)
        gmail_homepage.verify_url()

        gmail_homepage.click_compose_button()
        compose_page = ComposePage(self.driver)
        compose_page.type_recipient_address("test90@onet.pl")
        compose_page.type_title("selenium test")
        compose_page.type_message("testing")
        compose_page.click_send_button()
        gmail_homepage.open_sent_category()
        sent_page = SentPage(self.driver)
        sent_page.verify_text_presence("selenium test")


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestSendingEmail)
    unittest.TextTestRunner(verbosity=2).run(suite)
