import logging


class BasePage(object):

    def __init__(self, driver):

        self.driver = driver

    def open_url(self, domain):

        logging.warning("open page in browser")

        self.driver.get(domain)


